package com.gjw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication(scanBasePackages = {"com.gjw", "org.n3r.idworker"})
// 扫描mybatis mapper包路径
@MapperScan(basePackages = "com.gjw.mapper")
public class MuxinNettyApplication {

    public static void main(String[] args) {
        SpringApplication.run(MuxinNettyApplication.class, args);
    }

}
