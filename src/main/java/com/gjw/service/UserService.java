package com.gjw.service;

import com.gjw.pojo.Users;

/**
 * @author Administrator
 * @since 2019/8/2.
 */
public interface UserService {

    /**
     * 判断用户名是否存在
     *
     * @param username
     * @return
     */
    public boolean queryUsernameIsExist(String username);

    /**
     * 查询用户是否存在
     *
     * @param username
     * @param pwd
     * @return
     */
    public Users queryUserForLogin(String username, String pwd);

    /**
     * 用户注册
     *
     * @param users
     * @return
     */
    public Users saveUser(Users users);
}
