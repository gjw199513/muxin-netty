package com.gjw.mapper;

import com.gjw.pojo.Users;
import com.gjw.utils.MyMapper;

public interface UsersMapper extends MyMapper<Users> {
}