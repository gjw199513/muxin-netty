package com.gjw.mapper;

import com.gjw.pojo.ChatMsg;
import com.gjw.utils.MyMapper;

public interface ChatMsgMapper extends MyMapper<ChatMsg> {
}