package com.gjw.mapper;

import com.gjw.pojo.MyFriends;
import com.gjw.utils.MyMapper;

public interface MyFriendsMapper extends MyMapper<MyFriends> {
}