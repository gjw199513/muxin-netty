package com.gjw.mapper;

import com.gjw.pojo.FriendsRequest;
import com.gjw.utils.MyMapper;

public interface FriendsRequestMapper extends MyMapper<FriendsRequest> {
}